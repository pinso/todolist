# Projet TodoList#

Todolist est une application qui est inspiré de l’application de démonstration « Onsen Todo ». L’objectif est de faire évoluer cette application afin qu’elle soit dotée de fonctionnalités différentes ou supplémentaires.

### Fonctionnalités effectuées: ###

```
Fonctionnalités obligatoires
```

* Prérequis O1 : Récupérer et reconstruire l’application « Onsen Todo » sur votre environnement de développement.
* Prérequis O2 : Faire impérativement ressortir le nom du ou des participants sur les écrans de l’application. Voir le menu déroulant à gauche.
* Prérequis O3 : Modification de l’icône associée à l’application.
* Prérequis O4 : Modifications des liens le menu rétractable (switch menu).

```
Fonctionnalités prioritaires
```

* Question P1 : Assurer la persistance des données. Le local storage est fonctionnel sur la platforme browser mais n'a pas pu être testé sur Android. Pour ce faire, nous avons ajouter 3 méthodes dans service.js pour pouvoir manipuler les données.


* Question P2 : Ajout d’un nouvel état des tâches gérées. Pour pouvoir gérer cela, nous avons créer une nouvelle page html correspond au nouvel état souhaitait. C'est dans service.js que nous gérons le changement entre chaques états ainsi que les mises à jour.


* Question P3 : Suppression de toutes les tâches. Un bouton supprimer est possitionné en haut à droite (IOS) afin de supprimer toutes les tâches. Il est également possible de supprimer une unique tâche avec l’icône situé à droite d’une tâche. Dans controller.js, nous avons repris le même principe que pour accéder au menu.  


* Question P4 : Ajout d’un menu déroulant permettant de choisir une catégorie existante. C'est dans controller.js que nous avons créé une liste avec toutes les catégories.


### Information: ###

L’application n’a pas pu être essayé sur un mobile par manque de matériel. 


### Adresses mail: ###

Hoang Anh NGUYEN : hoang-anh.nguyen1@etu.univ-lorraine.fr  
Hugo PISANO : hugo.pisano7@etu.univ-lorraine.fr